define([
   'jquery',
   'customValidation',
   'jquery/validate',
   'validation',
   'mage/translate'
], function($) {
    "use strict",

    $.validator.addMethod(
        'validate-letter-whitespace', function (value) {
            return $.mage.isEmptyNoTrim(value) ||  /^[a-zA-ZÑñ\s]*$/.test(value);
    }, $.mage.__('Solo se aceptan letras (a-z o A-Z) y/o espacios en este campo.'));

    $.validator.addMethod(
        'validate-letter-digits-whitespace', function (value) {
            return $.mage.isEmptyNoTrim(value) ||  /^[a-zA-Z0-9Ññ\s]*$/.test(value);
    }, $.mage.__('Solo se aceptan letras (a-z o A-Z) n\xfameros (0-9) y/o espacios en este campo.'));

    $.validator.addMethod(
        'phoneCL', function (value) {
            if ($.trim(value) != '') {
                if (value.length < 8 || value.length > 9) {
                    return false;
                }
            }
            return $.mage.isEmptyNoTrim(value) ||  /^[29][0-9]*$/.test(value);
    }, $.mage.__('Solo se aceptan d\xedgitos con longitud entre 8 y 9 caracteres y deben iniciar en 2 o 9.'));

    $.validator.addMethod(
        'rutCL', function (value, element) {
            return this.optional(element) || validateRutCL(value);
    }, $.mage.__('D\xedgite un RUT v\xe1lido en este campo.'));

    $.validator.addMethod(
        'validate-digits', function (v) {
            return $.mage.isEmptyNoTrim(v) || !/[^\d]/.test(v);
    }, $.mage.__('D\xedgite un n\xfamero v\xe1lido en este campo.'));

    function validateRutCL (value) {
        if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test(value)) {
            return false;
        }
        var tmp = value.split('-');
        var digv = tmp[1];
        var rut = tmp[0];
        if (rut.length < 7 || rut.length > 8) {
            return false;
        }
        if (digv == 'K') {
            digv = 'k' ;
        }
        return (dv(rut) == digv);
    }

    function dv(T) {
        var M = 0, S = 1;
        for(; T; T=Math.floor(T/10))
            S = (S+T%10*(9-M++%6))%11;

        return S ? S-1 : 'k';
    }
});
