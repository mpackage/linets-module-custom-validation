<?php

namespace Linets\CustomValidation\Block\Widget;

class Name extends \Magento\Customer\Block\Widget\Name
{
    /**
     * @return void
     */
    public function _construct()
    {
        $this->setTemplate('Linets_CustomValidation::widget/name.phtml');
    }
}
